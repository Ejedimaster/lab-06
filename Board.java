public class Board {

	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board()
	{
		this.die1 = new Die();
		this.die2 = new Die();
		this.tiles = new boolean[12];
	}
	
	public boolean playATurn()
	{
		boolean value = false;
		
		die1.roll();
		die2.roll();
		
		System.out.println("Value of Dice 1: "+die1.getFaceValue());
		System.out.println("Value of Dice 2: "+die2.getFaceValue());
		
		int sumOfDice = die1.getFaceValue()+die2.getFaceValue();
		
		if(tiles[sumOfDice-1] == false)
		{
			tiles[sumOfDice-1] = true;
			System.out.println("Closing tile equal to sum: "+(sumOfDice));
		}
		else if(tiles[die1.getFaceValue()-1] == false)
		{
			tiles[die1.getFaceValue()-1]=true;
			System.out.println("Closing tile with the same value as die one: "+(die1.getFaceValue()));
		}
		else if(tiles[die2.getFaceValue()-1] == false)
		{
			tiles[die2.getFaceValue()-1]=true;
			System.out.println("Closing tile with the same value as die two: "+(die2.getFaceValue()));
		}
		else
		{
			System.out.println("All the tiles for these values are already shut");
			value = true;
		}
		
		return value;
	}
	
	public String toString()
	{
		String tileList = "";
		
		for(int i = 0; i < tiles.length; i++)
		{
			if(tiles[i] == false)
			{
				int a = i+1;
				tileList += " "+a;
			}
			else 
			{
				tileList += " X";
			}
		}
		return tileList;
	}

}