import java.util.Random;

public class Die {
	
	private int faceValue;
	private Random randomNumber;
	
	public Die() 
	{
		this.faceValue = 1;
		this.randomNumber = new Random();
	}
	
	public int getFaceValue() 
	{
		return this.faceValue;
	}
	
	public void roll()
	{
		this.faceValue = 1 + randomNumber.nextInt(6);
	}
	
	public String toString()
	{
		return "You rolled a: "+this.faceValue+" on the dice";
	}
	
}