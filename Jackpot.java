public class Jackpot {
	
	public static void main(String[] args)
	{
		System.out.println("Welcome to the Jackpot Game!");
		Die die1 = new Die();
		Die die2 = new Die();
		Board board1 = new Board();
		
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while(gameOver == false)
		{
			System.out.println(board1);
			board1.playATurn();
			if (board1.playATurn())
			{
				gameOver = true;
			}
			else
			{
				numOfTilesClosed = numOfTilesClosed + 1;
			}
		}
		
		if(numOfTilesClosed >= 7)
		{
			System.out.println("Congratulations! You have reached the Jackpot and Won the game!");
		}
		else
		{
			System.out.println("You've Lost the Game, Better Luck next Time!");
		}
		
	}
	
}